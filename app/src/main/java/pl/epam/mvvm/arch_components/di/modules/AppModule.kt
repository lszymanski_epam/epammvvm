package pl.epam.mvvm.arch_components.di.modules

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import pl.epam.mvvm.arch_components.app.App
import pl.epam.mvvm.common.di.scopes.AppScope


@Module
class AppModule {

    @Provides
    @AppScope
    fun providePreferences(application: App): SharedPreferences {
        return application.getSharedPreferences("DataStore", Context.MODE_PRIVATE)
    }

}
