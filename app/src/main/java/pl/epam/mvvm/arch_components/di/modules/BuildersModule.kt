package pl.epam.mvvm.arch_components.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.epam.mvvm.arch_components.screens.main.MainActivity

/**
 * Binds all sub-components within the app.
 */

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

}