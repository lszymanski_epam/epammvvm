package pl.epam.mvvm.arch_components.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import pl.epam.mvvm.R
import pl.epam.mvvm.common.base.BaseActivity
import pl.epam.mvvm.common.base.BaseRecyclerAdapter


class TimestampsAdapter (
        baseActivity: BaseActivity
) : BaseRecyclerAdapter<Long, TimestampsAdapter.ViewHolder>(baseActivity) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.i_timestamp, parent, false))
    }

    override fun setupHolder(item: Long, holder: ViewHolder) {
        holder.tvValue.text = "$item"
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvValue: TextView = itemView.findViewById(R.id.tv_value)
    }
}