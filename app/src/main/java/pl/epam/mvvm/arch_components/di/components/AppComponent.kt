package pl.epam.mvvm.arch_components.di.components

import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import pl.epam.mvvm.arch_components.app.App
import pl.epam.mvvm.arch_components.di.modules.AppModule
import pl.epam.mvvm.arch_components.di.modules.BuildersModule
import pl.epam.mvvm.arch_components.di.modules.DataModelModule
import pl.epam.mvvm.common.di.scopes.AppScope


@AppScope
@Component(modules = [
    AndroidSupportInjectionModule::class,
    BuildersModule::class,
    AppModule::class,
    DataModelModule::class
])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}
