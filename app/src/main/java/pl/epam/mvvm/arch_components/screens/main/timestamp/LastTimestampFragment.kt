package pl.epam.mvvm.arch_components.screens.main.timestamp

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.f_timestamp_last.*
import pl.epam.mvvm.R
import pl.epam.mvvm.common.base.BaseFragment
import pl.epam.mvvm.common.viewmodels.TimestampViewModel
import java.util.Date


class LastTimestampFragment: BaseFragment() {

    private lateinit var timestampViewModel: TimestampViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.f_timestamp_last, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupView()
    }

    private fun setupView() {
        timestampViewModel = ViewModelProviders.of(activity!!)
                .get(TimestampViewModel::class.java)

        timestampViewModel.lastTimestamp.observe(this, Observer {
            tv_last_timestamp.text = "${it ?: ""}"
        })

        btn_add.setOnClickListener {
            timestampViewModel.addTimestamp(Date())
        }
    }
}