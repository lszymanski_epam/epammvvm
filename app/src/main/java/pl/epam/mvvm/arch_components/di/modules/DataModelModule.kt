package pl.epam.mvvm.arch_components.di.modules

import dagger.Module
import dagger.Provides
import pl.epam.mvvm.common.datamodels.ITimestampDataModel
import pl.epam.mvvm.common.datamodels.TimestampDataModel
import pl.epam.mvvm.common.di.scopes.AppScope


@Module
class DataModelModule {

    @Provides
    @AppScope
    internal fun provideTimestampDataModel(): ITimestampDataModel =
            TimestampDataModel()

}
