package pl.epam.mvvm.arch_components.screens.main.timestamp

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.f_timestamp_list.*
import pl.epam.mvvm.R
import pl.epam.mvvm.arch_components.adapters.TimestampsAdapter
import pl.epam.mvvm.common.base.BaseFragment
import pl.epam.mvvm.common.viewmodels.TimestampViewModel


class TimestampListFragment: BaseFragment() {

    private lateinit var timestampViewModel: TimestampViewModel
    private var timestampsAdapter: TimestampsAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.f_timestamp_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupView()
    }

    private fun setupView() {
        timestampViewModel = ViewModelProviders.of(activity!!)
                .get(TimestampViewModel::class.java)
        setupAdapter()
        setupRecyclerView()
    }

    private fun setupAdapter() {
        timestampsAdapter = TimestampsAdapter(baseActivity!!)

        timestampViewModel.timestamps.observe(this, Observer {
            timestampsAdapter?.setItems(it!!)
        })
    }

    private fun setupRecyclerView() {
        rv_timestamps.adapter = timestampsAdapter
        rv_timestamps.addItemDecoration(DividerItemDecoration(baseActivity, DividerItemDecoration.VERTICAL))
    }
}