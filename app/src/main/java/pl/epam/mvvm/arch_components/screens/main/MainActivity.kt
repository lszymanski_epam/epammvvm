package pl.epam.mvvm.arch_components.screens.main


import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.a_main_wrapper.*
import pl.epam.mvvm.R
import pl.epam.mvvm.arch_components.screens.main.timestamp.LastTimestampFragment
import pl.epam.mvvm.arch_components.screens.main.timestamp.TimestampListFragment
import pl.epam.mvvm.common.base.BaseActivity
import pl.epam.mvvm.common.viewmodels.TimestampViewModel
import pl.epam.mvvm.common.viewmodels.TimestampViewModelFactory
import java.util.Date
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    internal lateinit var viewModelFactory: TimestampViewModelFactory

    private lateinit var timestampViewModel: TimestampViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_main_wrapper)
        setSupportActionBar(toolbar)

        timestampViewModel = ViewModelProviders.of(this, viewModelFactory).get(TimestampViewModel::class.java)

        setupView()

        openFragment(R.id.container, LastTimestampFragment(), false)
        openFragment(R.id.container_list, TimestampListFragment(), false)
    }

    private fun setupView() {
        setupButtons()
    }

    private fun setupButtons() {
        fab.setOnClickListener {
            timestampViewModel.addTimestamp(Date())
        }
    }

}
