package pl.epam.mvvm.common.datamodels

import android.arch.lifecycle.MutableLiveData
import java.util.Date


open class TimestampDataModel: ITimestampDataModel {

    override val timestamps = MutableLiveData<List<Long>>().also {
        it.value = listOf()
    }

    override val variable = MutableLiveData<String>().also {
        it.value = ""
    }

    override fun addTimestamp(date: Date) {
        timestamps.value = timestamps.value!!.plus(date.time)
    }
}
