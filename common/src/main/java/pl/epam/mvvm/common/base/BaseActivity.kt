package pl.epam.mvvm.common.base

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.InputMethodManager

abstract class BaseActivity : AppCompatActivity() {

    /**
     * Replaces current fragment in container
     *
     * @param containerId       the container view id to replace
     * @param fragment          the fragment to attach to R.id.container (replacing previous if any)
     * @param addToBackStack    call [FragmentTransaction.addToBackStack] if true
     */
    fun openFragment(containerId: Int, fragment: Fragment, addToBackStack: Boolean) {
        if (!isFinishing) {
            hideKeyboard()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(containerId, fragment, fragment.javaClass.name)
            if (addToBackStack)
                transaction.addToBackStack(fragment.javaClass.name)
            transaction.commitAllowingStateLoss()
        }
    }

    fun closeFragment(containerId: Int) {
        if (!isFinishing) {
            val fragment = supportFragmentManager.findFragmentById(containerId)
            if (fragment != null) {
                supportFragmentManager.beginTransaction()
                        .remove(fragment)
                        .commitAllowingStateLoss()
            }
        }
    }

    fun clearBackStack() {
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    private fun hideKeyboard() {
        try {
            val inputManager = applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(window.decorView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
