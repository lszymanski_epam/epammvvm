package pl.epam.mvvm.common.viewmodels


import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import pl.epam.mvvm.common.datamodels.ITimestampDataModel
import java.util.Date

class TimestampViewModel(
        private val timestampDataModel: ITimestampDataModel
) : ViewModel() {

    val timestamps = timestampDataModel.timestamps

    val lastTimestamp: LiveData<Long> = Transformations.map(timestamps) {
        timestampsList -> timestampsList.lastOrNull()
    }

    val variable = timestampDataModel.variable

    val variableLength: LiveData<Int> = Transformations.map(variable) {
        v -> v.length
    }

    fun addTimestamp(date: Date) {
        timestampDataModel.addTimestamp(date)
    }

    fun addTimestamp() {
        addTimestamp(Date())
    }
}