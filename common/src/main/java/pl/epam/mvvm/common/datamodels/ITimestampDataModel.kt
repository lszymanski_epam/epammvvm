package pl.epam.mvvm.common.datamodels

import android.arch.lifecycle.MutableLiveData
import java.util.Date


interface ITimestampDataModel {

    val timestamps: MutableLiveData<List<Long>>
    val variable: MutableLiveData<String>

    fun addTimestamp(date: Date)

}
 