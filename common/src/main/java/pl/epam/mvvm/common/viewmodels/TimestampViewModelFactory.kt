package pl.epam.mvvm.common.viewmodels

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import pl.epam.mvvm.common.datamodels.ITimestampDataModel
import javax.inject.Inject

class TimestampViewModelFactory
        @Inject
        constructor(
                val timestampDataModel: ITimestampDataModel
): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TimestampViewModel::class.java)) {
            return TimestampViewModel(timestampDataModel) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}