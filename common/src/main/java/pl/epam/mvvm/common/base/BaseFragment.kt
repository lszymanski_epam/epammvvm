package pl.epam.mvvm.common.base

import android.content.Context
import android.support.v4.app.Fragment

abstract class BaseFragment: Fragment() {
    protected var baseActivity: BaseActivity? = null


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            baseActivity = context as BaseActivity?
        } catch (ex: ClassCastException) {
            throw ClassCastException("Activity must be of class " + BaseActivity::class.java.simpleName)
        }
    }
}