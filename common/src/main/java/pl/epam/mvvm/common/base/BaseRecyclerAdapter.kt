package pl.epam.mvvm.common.base

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

abstract class BaseRecyclerAdapter<T, VH : RecyclerView.ViewHolder>(
        protected val activity: BaseActivity
) : RecyclerView.Adapter<VH>() {

    protected val inflater: LayoutInflater = LayoutInflater.from(activity)
    protected var objects: List<T> = listOf()

    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH

    override fun onBindViewHolder(holder: VH, position: Int) {
        val item = getItem(holder.adapterPosition)
        setupHolder(item, holder)
    }

    protected abstract fun setupHolder(item: T, holder: VH)

    open fun setItems(items: List<T>) {
        objects = items
        notifyDataSetChanged()
    }

    open fun clear() {
        val size = objects.size
        objects = listOf()
        notifyItemRangeRemoved(0, size)
    }

    open fun getItem(position: Int): T {
        return objects[position]
    }

    operator fun contains(item: T): Boolean {
        return objects.contains(item)
    }

    override fun getItemCount(): Int {
        return objects.size
    }
}
