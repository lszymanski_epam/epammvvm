package pl.epam.mvvm.common.viewmodels

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.*
import org.junit.Assert.assertEquals
import org.junit.ClassRule
import org.junit.Test
import pl.epam.mvvm.common.datamodels.ITimestampDataModel
import java.util.Calendar
import java.util.Date


class TimestampViewModelTest {

    companion object {

        @ClassRule
        @JvmField
        val rule = InstantTaskExecutorRule()
    }

    private val timestampDataModel = mock<ITimestampDataModel>()
    private val observer = mock<Observer<List<Long>>>()
    private val observerLastTimestamp = mock<Observer<Long>>()

    @Test
    fun testGetTimestampsList() {
        val expectedTimestamps = listOf<Long>(createDate(1).time,
                createDate(2).time,
                createDate(3).time,
                createDate(4).time,
                createDate(5).time)
        val mockedTimestamps = MutableLiveData<List<Long>>().also {
            it.value = expectedTimestamps
        }
        whenever(timestampDataModel.timestamps).thenReturn(mockedTimestamps)

        val viewModel = TimestampViewModel(timestampDataModel)
        viewModel.timestamps.observeForever(observer)

        assertEquals(expectedTimestamps, viewModel.timestamps.value)
    }

    @Test
    fun testGetLastTimestamp() {
        val mockedTimestamps = MutableLiveData<List<Long>>().also {
            it.value = listOf(1, 2, 3)
        }
        whenever(timestampDataModel.timestamps).thenReturn(mockedTimestamps)

        val viewModel = TimestampViewModel(timestampDataModel)
        viewModel.lastTimestamp.observeForever(observerLastTimestamp)

        assertEquals(mockedTimestamps.value!!.last(), viewModel.lastTimestamp.value)
    }

    @Test
    fun testGetVariable() {
        val observer = mock<Observer<String>>()
        val expectedValue = "12345"
        val mockedVariable = MutableLiveData<String>().also {
            it.value = expectedValue
        }
        whenever(timestampDataModel.variable).thenReturn(mockedVariable)

        val viewModel = TimestampViewModel(timestampDataModel)
        viewModel.variable.observeForever(observer)

        assertEquals(expectedValue, viewModel.variable.value)
    }

    @Test
    fun testGetVariableLength() {
        val observer = mock<Observer<Int>>()
        val expectedValue = "12345"
        val mockedVariable = MutableLiveData<String>().also {
            it.value = expectedValue
        }
        whenever(timestampDataModel.variable).thenReturn(mockedVariable)

        val viewModel = TimestampViewModel(timestampDataModel)
        viewModel.variableLength.observeForever(observer)

        assertEquals(expectedValue.length, viewModel.variableLength.value)
    }

    @Test
    fun testAddTimestamp() {
        val date = createDate(1)

        val viewModel = TimestampViewModel(timestampDataModel)
        viewModel.addTimestamp(date)

        val inOrder = inOrder(timestampDataModel)
        inOrder.verify(timestampDataModel).addTimestamp(date)
        inOrder.verifyNoMoreInteractions()
    }

    private fun createDate(day: Int): Date {
        val calendar = Calendar.getInstance()

        calendar.set(Calendar.YEAR, 2018)
        calendar.set(Calendar.MONTH, 1)
        calendar.set(Calendar.DATE, day)

        return calendar.time
    }
}