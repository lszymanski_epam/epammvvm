package pl.epam.mvvm.common.datamodels

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.inOrder
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import org.junit.Assert.assertEquals
import org.junit.ClassRule
import org.junit.Test
import java.util.*

class TimestampDataModelTest {

    companion object {

        @ClassRule
        @JvmField
        val rule = InstantTaskExecutorRule()
    }

    private val timestampDataModel = TimestampDataModel()
    private val observer = mock<Observer<List<Long>>>()
    private val observerVariable = mock<Observer<String>>()

    @Test
    fun testAddTimestamp() {
        val date = Date()
        val expectedList = listOf(date.time)

        timestampDataModel.timestamps.observeForever(observer)
        timestampDataModel.addTimestamp(date)

        val inOrder = inOrder(observer)
        inOrder.verify(observer).onChanged(expectedList)
        inOrder.verifyNoMoreInteractions()
        assertEquals(expectedList, timestampDataModel.timestamps.value)
    }

    @Test
    fun testAddTimestampAdvanced() {
        val expectedDates = mutableListOf<Date>()
        val expectedTimestamps = mutableListOf<Long>()
        listOf(1, 2, 3, 4, 5)
                .forEach {
                    val date = createDate(it)
                    expectedDates.add(date)
                    expectedTimestamps.add(date.time)
                }

        timestampDataModel.timestamps.observeForever(observer)
        expectedDates
                .forEach {
                    timestampDataModel.addTimestamp(it)
                }

        val inOrder = inOrder(observer)
        inOrder.verify(observer).onChanged(listOf())
        inOrder.verify(observer, times(5)).onChanged(any())
        inOrder.verifyNoMoreInteractions()
        assertEquals(expectedTimestamps, timestampDataModel.timestamps.value)
    }

    @Test
    fun testVariable() {
        val expectedValue = "12345"

        timestampDataModel.variable.observeForever(observerVariable)

        timestampDataModel.variable.value = expectedValue

        val inOrder = inOrder(observerVariable)
        inOrder.verify(observerVariable).onChanged(expectedValue)
        inOrder.verifyNoMoreInteractions()
        assertEquals(expectedValue, timestampDataModel.variable.value)
    }

    private fun createDate(day: Int): Date {
        val calendar = Calendar.getInstance()

        calendar.set(Calendar.YEAR, 2018)
        calendar.set(Calendar.MONTH, 1)
        calendar.set(Calendar.DATE, day)

        return calendar.time
    }
}