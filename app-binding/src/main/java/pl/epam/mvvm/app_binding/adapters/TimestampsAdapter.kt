package pl.epam.mvvm.app_binding.adapters

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import pl.epam.mvvm.app_binding.R
import pl.epam.mvvm.app_binding.databinding.ITimestampBinding
import pl.epam.mvvm.common.base.BaseActivity
import pl.epam.mvvm.common.base.BaseRecyclerAdapter


class TimestampsAdapter (
        baseActivity: BaseActivity
) : BaseRecyclerAdapter<Long, TimestampsAdapter.ViewHolder>(baseActivity) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ITimestampBinding>(layoutInflater,
                R.layout.i_timestamp, parent, false)
        return ViewHolder(binding)
    }

    override fun setupHolder(item: Long, holder: ViewHolder) {
        holder.bind(item)
    }

    class ViewHolder(private val binding: ITimestampBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Long) {
            binding.itemValue = item
            binding.executePendingBindings()   // update the view now
        }
    }
}