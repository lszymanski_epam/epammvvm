package pl.epam.mvvm.app_binding.utils

import android.databinding.BindingAdapter
import android.databinding.InverseBindingAdapter
import android.databinding.InverseBindingListener
import android.databinding.adapters.ListenerUtil
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import pl.epam.mvvm.app_binding.R
import pl.epam.mvvm.app_binding.adapters.TimestampsAdapter
import pl.epam.mvvm.common.base.BaseActivity


@BindingAdapter("data")
fun bind(recyclerView: RecyclerView, data: List<Long>) {
    val adapter = TimestampsAdapter(recyclerView.context as BaseActivity)
    adapter.setItems(data)
    recyclerView.adapter = adapter
}

@BindingAdapter(value = ["textAttrChanged"])
fun setTextWatcher(view: EditText, inverseBindingListener: InverseBindingListener) {
    val newTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            inverseBindingListener.onChange()
        }

        override fun afterTextChanged(p0: Editable?) {}
    }
    val oldTextWatcher = ListenerUtil.trackListener(view, newTextWatcher, R.id.textWatcher)
    if (oldTextWatcher != null) {
        view.removeTextChangedListener(oldTextWatcher)
    }
    view.addTextChangedListener(newTextWatcher)
}

// fun setTextWatcher() is not necessary if we use android event:
// @InverseBindingAdapter(attribute = "android:text", event = "android:textAttrChanged")
@InverseBindingAdapter(attribute = "android:text", event = "textAttrChanged")
fun getText(view: EditText): String? {
    return view.text.toString()
}

@BindingAdapter(value = ["android:text"])
fun setText(view: EditText, text: String?) {
    val textLength = text?.length ?: 0
    when (textLength) {
        in 14..15 ->  view.setBackgroundColor(Color.RED)
        in 8..13 ->  view.setBackgroundColor(Color.GREEN)
        else ->  view.setBackgroundColor(Color.WHITE)
    }
    if (view.text.toString() != text) {
        view.setText(text)
    }
}