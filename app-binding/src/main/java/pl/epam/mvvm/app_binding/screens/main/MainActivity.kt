package pl.epam.mvvm.app_binding.screens.main


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import dagger.android.AndroidInjection
import pl.epam.mvvm.app_binding.R
import pl.epam.mvvm.app_binding.databinding.AMainWrapperBinding
import pl.epam.mvvm.app_binding.screens.main.timestamp.LastTimestampFragment
import pl.epam.mvvm.app_binding.screens.main.timestamp.TimestampListFragment
import pl.epam.mvvm.common.base.BaseActivity
import pl.epam.mvvm.common.viewmodels.TimestampViewModel
import pl.epam.mvvm.common.viewmodels.TimestampViewModelFactory
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    internal lateinit var viewModelFactory: TimestampViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<AMainWrapperBinding>(this, R.layout.a_main_wrapper)
        binding.setLifecycleOwner(this)
        binding.viewmodel = ViewModelProviders.of(this, viewModelFactory).get(TimestampViewModel::class.java)

        setSupportActionBar(binding.toolbar)

        openFragment(R.id.container, LastTimestampFragment(), false)
        openFragment(R.id.container_list, TimestampListFragment(), false)
    }

}
