package pl.epam.mvvm.app_binding.di.components

import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import pl.epam.mvvm.app_binding.app.App
import pl.epam.mvvm.app_binding.di.modules.AppModule
import pl.epam.mvvm.app_binding.di.modules.BuildersModule
import pl.epam.mvvm.app_binding.di.modules.DataModelModule
import pl.epam.mvvm.common.di.scopes.AppScope


@AppScope
@Component(modules = [
    AndroidSupportInjectionModule::class,
    BuildersModule::class,
    AppModule::class,
    DataModelModule::class
])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}
