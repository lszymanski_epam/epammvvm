package pl.epam.mvvm.app_binding.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.epam.mvvm.app_binding.screens.main.MainActivity

/**
 * Binds all sub-components within the app.
 */

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

}