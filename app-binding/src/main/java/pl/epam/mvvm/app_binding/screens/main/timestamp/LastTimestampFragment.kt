package pl.epam.mvvm.app_binding.screens.main.timestamp

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pl.epam.mvvm.app_binding.R
import pl.epam.mvvm.app_binding.databinding.FTimestampLastBinding
import pl.epam.mvvm.common.base.BaseFragment
import pl.epam.mvvm.common.viewmodels.TimestampViewModel


class LastTimestampFragment: BaseFragment() {

    private lateinit var binding: FTimestampLastBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.f_timestamp_last, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.viewmodel = ViewModelProviders.of(activity!!).get(TimestampViewModel::class.java)
    }

}