package pl.epam.mvvm.app_binding.screens.main.timestamp

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pl.epam.mvvm.app_binding.R
import pl.epam.mvvm.app_binding.databinding.FTimestampListBinding
import pl.epam.mvvm.common.base.BaseFragment
import pl.epam.mvvm.common.viewmodels.TimestampViewModel


class TimestampListFragment: BaseFragment() {

    private lateinit var binding: FTimestampListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.f_timestamp_list, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupView()
    }

    private fun setupView() {
        binding.viewmodel = ViewModelProviders.of(activity!!).get(TimestampViewModel::class.java)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        binding.rvTimestamps.addItemDecoration(DividerItemDecoration(baseActivity, DividerItemDecoration.VERTICAL))
    }
}